  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <a href="<?=base_url('pegawai-permohonan-cuti');?>" class="btn btn-default btn-lg"><i class="fa fa-fw fa-file-text-o"></i> Absensi Dinas Luar</a>
    </section>

    <!-- Main content -->
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-12">
        <div class="box-body box-profile">
          <div class="box box-primary">
            <div class="box-header">
              <?php echo $map['html']; ?>
            </div>
            <div class="box-body box-profile">

              <!--<strong><i class="fa fa-book margin-r-5"></i> Alamat IP</strong>-->
                <!--<p class="text-muted"><?php echo $_SERVER['REMOTE_ADDR'];?> </p>-->
              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Longtitude</strong>
                <p class="text-muted"><?=$this->session->userdata('p_nip');?></p>
              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Latitude</strong>
                <p class="text-muted"><?=$this->session->userdata('p_kota');?>,<?=$this->session->userdata('p_alamat');?></p>
              <hr>
            <div class="box-footer">
              <a href="<?=base_url('pegawai-logout');?>" class="btn btn-warning btn-block"><b>Log out</b></a>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </section>
    <?php echo $map['js']; ?>
    <!-- /.content -->
    <script type="text/javascript">
      $(document).ready(function(){

 

      });
    </script>
