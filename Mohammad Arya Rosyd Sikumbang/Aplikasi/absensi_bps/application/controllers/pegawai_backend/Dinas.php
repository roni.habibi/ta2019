<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dinas extends CI_Controller {
  function __construct(){
    parent::__construct();

    if($this->session->userdata('pegawai_login') == 0){
      redirect(base_url('pegawai-login'));
    }
    $this->load->library('Googlemaps');

    $this->load->model('Mpegawai');

  }

  function index()
  {
       $config = array(
      'center'         => '37.4419, -122.1419', // Center of the map
      'zoom'           => '10', // Map zoom
      'placesAutocompleteInputID' => 'myPlaceTextBox',
      'placesAutocompleteBoundsMap' => TRUE, // set results biased towards the maps viewport
      'placesAutocompleteOnChange' => 'alert(\'You selected a place\');',
      );
    $this->googlemaps->initialize($config);
    $marker = array(
      'position'  =>'37.4419, -122.1419', // Marker Location
      'draggable' => true,
      'animation' => 'DROP',
      );
    $this->googlemaps->add_marker($marker); 

    $data['map'] =  $this->googlemaps->create_map();

    $this->load->view('template/header_pegawai');
    $this->load->view('pegawai/dinas', $data);
    $this->load->view('template/footer_pegawai');
  }

}
