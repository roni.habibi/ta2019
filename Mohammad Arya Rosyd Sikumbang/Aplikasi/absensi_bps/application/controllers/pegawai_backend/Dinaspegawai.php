<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dinaspegawai extends CI_Controller {
  function __construct(){
  	parent::__construct();
    $this->load->library('Googlemaps');

  	if($this->session->userdata('pegawai_login') == 0){
  		redirect(base_url('pegawai-login'));
  	}

  	$this->load->model('Mpegawai');

  }

  function index()
  {
    $config = array(
      'center'         => '-6.914744','107.609810', // Center of the map
      'zoom'           => 'auto', // Map zoom
      'placesAutocompleteInputID' => 'myPlaceTextBox',
      'placesAutocompleteBoundsMap' => TRUE, // set results biased towards the maps viewport
      'placesAutocompleteOnChange' => 'alert(\'You selected a place\');',
      );
    $this->googlemaps->initialize($config);
    $marker = array(
      'position'  =>'-6.914744','107.609810', // Marker Location
      'draggable' => true,
      'animation' => 'DROP',
      );
    $this->googlemaps->add_marker($marker); 

    $data['map'] =  $this->googlemaps->create_map();

    $this->load->view('template/header_pegawai');
    $this->load->view('pegawai/dinas', $data);
    $this->load->view('template/footer_pegawai');
  	// $this->load->view('template/header_pegawai');
   //  $this->load->view('pegawai/akun');
   //  $this->load->view('template/footer_pegawai');
  }

  function maps()
  {
    $config = array(
      'center'         => $latitudes.",".$longitudes, // Center of the map
      'zoom'           => 'auto', // Map zoom
      'placesAutocompleteInputID' => 'myPlaceTextBox',
      'placesAutocompleteBoundsMap' => TRUE, // set results biased towards the maps viewport
      'placesAutocompleteOnChange' => 'alert(\'You selected a place\');',
      );
    $this->googlemaps->initialize($config);
    $marker = array(
      'position'  => $latitudes.",".$longitudes, // Marker Location
      'draggable' => true,
      'animation' => 'DROP',
      );
    $this->googlemaps->add_marker($marker); 

    
    
    $data['map'] =  $this->googlemaps->create_map();

    $this->load->view('template/header_pegawai');
    $this->load->view('pegawai/dinas',$data);
    $this->load->view('template/footer_pegawai');
  
  }

}
